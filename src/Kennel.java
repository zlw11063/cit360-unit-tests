public class Kennel {

    public DogBean[] buildDogs() {
        DogBean Dogs[] = new DogBean[5];
        Dogs[0] = new DogBean(4, "Blue",4.5, "German Shepard", "Hugo");
        Dogs[1] = new DogBean(4, "Yellow",3.5, "Collie", "Spud");
        Dogs[2] = new DogBean(4, "Brown",6.5, "Pit Bull", "Pudd");
        Dogs[3] = new DogBean(4, "Vermillion",7.5, "Pug", "Lard");
        Dogs[4] = new DogBean(4, "Tickle-Me-Pink", 0.1,"Husky", "Oreo");
        return Dogs;
    }

    public void displayDogs(DogBean showDogs[]) {
        for (DogBean Dog : showDogs) {
            String dogString = Dog.toString();
            System.out.println(dogString);
        }
    }

    public static void main(String[] args) {

        Kennel kennel = new Kennel();
        DogBean allDogs[] = kennel.buildDogs();
        kennel.displayDogs(allDogs);
    }
}
