public class DogBean extends MammalBean {
    private String breed;

    @Override
    public String toString() {
        return "DogBean{" +
                "breed='" + breed + '\'' +
                ", name='" + name + '\'' +
                ", legCount='" + getLegCount() + '\'' +
                ", color='" + getColor() + '\'' +
                ", height='" + getHeight() + '\'' +
                '}';
    }

    private String name;

    public DogBean(int animLegCount, String animalColor, double animalHeight, String animalBreed, String animalName) {
        super(animLegCount, animalColor, animalHeight);
        breed = animalBreed;
        name = animalName;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String newBreed) {
        breed = newBreed;
    }

    public String getName() {
        return name;
    }

    public void setName(String newName) {
        name = newName;
    }

    public void description() {
        System.out.println(toString());
        //System.out.println("legCount=" + getLegCount() + " color=" + getColor() + " height=" + getHeight() +
        //      " breed=" + getBreed() + " name=" + getName());
    }
}