public class MammalBean {

    private int legCount;
    private String color;
    private double height;

    public MammalBean(int animalLegCount, String animalColor, double animalHeight) {
        legCount = animalLegCount;
        color = animalColor;
        height = animalHeight;
    }

    public int getLegCount() {
        return legCount;
    }

    public void setLegCount(int count) {
        legCount = count;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String newColor) {
        color = newColor;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double newHeight) {
        height = newHeight;
    }



}
