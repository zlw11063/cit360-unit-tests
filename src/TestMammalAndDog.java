import org.junit.Test;

import java.util.*;

import static java.time.Duration.ofMinutes;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.*;

public class TestMammalAndDog {

    public MammalBean[] createMammalArray() {
        MammalBean mammals[] = {
                new MammalBean(4, "purple", 2.5),
                new MammalBean(5, "green", 3.5),
                new MammalBean(6, "pink", 2.8),
                new MammalBean(3, "transparent", 2.9)
        };
        return mammals;
    }

    public DogBean[] createDogArray() {
        DogBean dogs[] = {
                new DogBean(4, "purple", 2.5, "Poodle", "Chicken"),
                new DogBean(5, "green", 3.5, "Russian Shepard", "Moose"),
                new DogBean(6, "pink", 2.8, "Pug", "Cow"),
                new DogBean(1, "ugly", 2.2, "Chihuahua", "Hannibal"),
        };
        return dogs;
    }

    @Test
    public void testMammalParams() {
        // My Sample Legs (increments by 1 each new mammal)
        int legs = 0;
        // My Sample Color Strings
        String colors[] = {
                "blue",
                "red",
                "purple"
        };
        // My Sample Height (increments by 1 each new mammal)
        double height = 1.5;
        // My Sample Breeds
        String breeds[] = {
                "Poodle",
                "German Shepard",
                "Pug"
        };
        // My Sample Names
        String names[] = {
                "Couch",
                "Lamp",
                "Hot Mess"
        };
        for (int x = 0; x < 3; x++) {
            MammalBean mammal = new MammalBean(x, colors[x], height);
            assertEquals(legs, mammal.getLegCount());
            assertEquals(colors[x], mammal.getColor());
            assertEquals(height, mammal.getHeight(), 0.05);
            legs++;
            height++;
        }

        for (int y = 0; y < 3; y++) {
            DogBean dog = new DogBean(legs, colors[y], height, breeds[y], names[y]);
            assertEquals(legs, dog.getLegCount());
            assertEquals(colors[y], dog.getColor());
            assertEquals(height, dog.getHeight(), 0.05);
            assertEquals(breeds[y], dog.getBreed());
            assertEquals(names[y], dog.getName());
            legs++;
            height++;
        }
    }

    @Test
    public void testMammalSet() {
        // Create our new HashSet
        Set<MammalBean> mamSet = new HashSet<MammalBean>();
        // Check that it was created (just to learn this test function)
        assertNotNull(mamSet);
        // Create our array of mammals to add to the set
        MammalBean[] mammals = createMammalArray();
        //
        // Add all of our mammals into the set
        Collections.addAll(mamSet, mammals);
        // Check if our herd made it into the set
        assertTrue(mamSet.contains(mammals[0]));
        assertTrue(mamSet.contains(mammals[1]));
        assertTrue(mamSet.contains(mammals[2]));
        assertTrue(mamSet.contains(mammals[3]));
        // Remove two of the mammals
        mamSet.remove(mammals[0]);
        mamSet.remove(mammals[1]);
        // Check the size
        assertEquals(2, mamSet.size());
        // Check out last two mammals are still in there
        assertTrue(mamSet.contains(mammals[2]));
        assertTrue(mamSet.contains(mammals[3]));
        // Check for the ones we removed
        assertFalse(mamSet.contains(mammals[0]));
        assertFalse(mamSet.contains(mammals[1]));
    }

    @Test
    public void testDogSet() {
        // Create our new Map
        Map<String, DogBean> dogMap = new HashMap<>();
        // Create our array of dogs to add to the map
        DogBean[] dogs = createDogArray();
        // Add all of our dogs into the set
        for (DogBean dog : dogs) {
            dogMap.put(dog.getName(), dog);
        }
        // Check if still empty (because why not?)
        assertFalse(dogMap.isEmpty());
        // Check size to see if all four were added
        // Check this before individual because we'll know one is false if this is
        assertEquals(4, dogMap.size());
        // Check if our pack made it into the set
        assertTrue(dogMap.containsValue(dogs[0]));
        assertTrue(dogMap.containsValue(dogs[1]));
        assertTrue(dogMap.containsValue(dogs[2]));
        assertTrue(dogMap.containsValue(dogs[3]));
        assertTrue(dogMap.containsKey(dogs[0].getName()));
        assertTrue(dogMap.containsKey(dogs[1].getName()));
        assertTrue(dogMap.containsKey(dogs[2].getName()));
        assertTrue(dogMap.containsKey(dogs[3].getName()));
        // Remove two of the dogs
        dogMap.remove(dogs[0].getName());
        dogMap.remove(dogs[1].getName());
        // Check the size
        assertEquals(2, dogMap.size());
        // Check out last two dogs are still in there
        assertTrue(dogMap.containsValue(dogs[2]));
        assertTrue(dogMap.containsValue(dogs[3]));
        assertTrue(dogMap.containsKey(dogs[2]));
        assertTrue(dogMap.containsKey(dogs[3]));
        // Check if our two dogs were actually removed
        assertFalse(dogMap.containsValue(dogs[0]));
        assertFalse(dogMap.containsValue(dogs[1]));
        assertFalse(dogMap.containsKey(dogs[0].getName()));
        assertFalse(dogMap.containsKey(dogs[1].getName()));
    }

    @Test
    public void testCreatedDogArray() {
        // This is for me to learn how to test arrays
        /*
        This test fails, but only because a space occurs at the end of the first dog in the EXPECTED
        array. Otherwise, it is fine. I can't figure out why a space is being added. This isn't required
        for the assignment, but I got curious.
         */
        DogBean dogs[] = {
                new DogBean(4, "purple", 2.5, "Poodle", "Chicken"),
                new DogBean(5, "green", 3.5, "Russian Shepard", "Moose"),
                new DogBean(6, "pink", 2.8, "Pug", "Cow"),
                new DogBean(1, "ugly", 2.2, "Chihuahua", "Hannibal")};
        assertArrayEquals(dogs, createDogArray());
    }

}
